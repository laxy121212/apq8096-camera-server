/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef __EXPGAIN_INTERFACE_MODALAI__
#define __EXPGAIN_INTERFACE_MODALAI__

#include "common_defs.h"
#include "expgain_interface.h"
#include "exposure-hist.h"

using namespace std;

//------------------------------------------------------------------------------------------------------------------------------
// Main interface class to manage all the cameras
//------------------------------------------------------------------------------------------------------------------------------
class ExpGainModalAI : public ExpGainInterface
{
public:
    ExpGainModalAI() 
    { 
        currentExposureNs = 0;
        currentGain = 0;    
        validValues = false;

        width = 0;
        height = 0;
        format = 0;
        strideInPixels = 0;
    }
    virtual ~ExpGainModalAI() { Destroy(); }
    // Perform any one time initialization
    virtual Status Initialize(const ExpGainInterfaceData* pIntfData);
    // Add a frame to be used to do the exposure/gain
    virtual Status GetNewExpGain(
        const ExpGainFrameData* pNewFrame, ExpGainResult* pNewExpGain);

private:

    // Destroy the instance
    void Destroy();

    uint64_t currentExposureNs;
    int16_t  currentGain;
    bool     validValues;

    uint32_t           width;               ///< Width of the frame
    uint32_t           height;              ///< Height of the frame
    uint32_t           format;              ///< Format of the frame
    uint32_t           strideInPixels;      ///< Stride in pixels

    // Histogram exposure module
    ModalExposureHist*    exposure;
};

#endif // __EXPGAIN_INTERFACE_INTERNAL__
