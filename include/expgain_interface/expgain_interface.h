/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef EXPGAIN_INTERFACE_H
#define EXPGAIN_INTERFACE_H

#include <stdint.h>
#include <stddef.h>
#include "common_defs.h"

// Function prototypes for callbacks by the exposure/gain interface into the API specific camera manager implementation
// API specific meaning hal3, v4l2 or anything else
// @todo remove this - dont think this is required
typedef void (*NewExpGainValuesCb)(int gain, int64_t exposure);

// -----------------------------------------------------------------------------------------------------------------------------
// Initialization data to be provided to the exposure/gain interface
// -----------------------------------------------------------------------------------------------------------------------------
struct ExpGainInterfaceData
{
    NewExpGainValuesCb pfNewExpGainValues;  ///< Callback into the module that creates this interface
    uint32_t           width;               ///< Width of the frame
    uint32_t           height;              ///< Height of the frame
    uint32_t           format;              ///< Format of the frame
    uint32_t           strideInPixels;      ///< Stride in pixels
    const void*        pAlgoSpecificData;   ///< Void pointer to algorithm specific data
    const char*        cameraType;          ///< String type of camera to derive pipe/interface name
    int                channel;
};

// -----------------------------------------------------------------------------------------------------------------------------
// Frame information to be given to the exposure/gain interface
// -----------------------------------------------------------------------------------------------------------------------------
struct ExpGainFrameData
{
    const uint8_t* pFramePixels;    ///< Frame pixel data
};

// -----------------------------------------------------------------------------------------------------------------------------
// Result from the Exposure/Gain interface
// -----------------------------------------------------------------------------------------------------------------------------
struct ExpGainResult
{
    int64_t exposure;   ///< The new exposure value to set
    int32_t gain;       ///< The new gain value to set
};

//------------------------------------------------------------------------------------------------------------------------------
// Abstract base class that provides an interface to get exposure/gain values
//
// The user of this interface is agnostic of the internal details. The implementation of the interface in this class could be
// done using any algorithm indicated by "enum CameraAEAlgo"
//------------------------------------------------------------------------------------------------------------------------------
class ExpGainInterface
{
public:
    // Initialize the instance
    virtual Status Initialize(const ExpGainInterfaceData* pIntfData) = 0;
    virtual Status GetNewExpGain(const ExpGainFrameData* pNewFrame, ExpGainResult* pNewExpGain) = 0;

    // Disable direct instantiation of this class
    ExpGainInterface() {}
    virtual ~ExpGainInterface() {}

protected:

    ExpGainInterfaceData m_interfaceData;  ///< Any data pertaining to the server core
};

#endif // end #define EXPGAIN_INTERFACE_H
