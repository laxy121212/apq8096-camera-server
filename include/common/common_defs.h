/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/
#ifndef COMMON_DEFS_H
#define COMMON_DEFS_H

#include "debug_log.h"
#include "exposure-hist.h"

// Forward declaration
struct camera_image_metadata_t;
struct point_cloud_metadata_t;
struct tof_data_t;

#define PADDING_DISABLED __attribute__((packed))

static const int INT_INVALID_VALUE   = 0xdeadbeef;
static const int MAX_NAME_LENGTH     = 128;

#define PREVIEW_CH_OFFSET 0

#define NUM_TOF_CHANNELS  6
#define IR_CH_OFFSET      0
#define DEPTH_CH_OFFSET   1
#define CONF_CH_OFFSET    2
#define NOISE_CH_OFFSET   3
#define PC_CH_OFFSET      4
#define FULL_CH_OFFSET    5

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
enum StreamType
{
    StreamTypePreview = 0,      ///< Preview stream
    StreamTypeVideo,            ///< Video stream
    StreamTypeSnapshot,         ///< Snapshot stream
    StreamTypeMax,
};

// -----------------------------------------------------------------------------------------------------------------------------
// Supported stream types
// -----------------------------------------------------------------------------------------------------------------------------
typedef enum
{
    // RAW only mode for devices that will simultaneously use more than two cameras.
    // This mode has following limitations: Back end 3A, Face Detect or any additional functionality depending on image/sensor
    // statistics and YUV streams will be disabled

    QCAMERA3_VENDOR_STREAM_CONFIGURATION_RAW_ONLY_MODE = 0x8000,
} QCamera3VendorStreamConfiguration;

//------------------------------------------------------------------------------------------------------------------------------
// Status values that we should use everywhere instead of magic numbers like 0, -1 etc
//------------------------------------------------------------------------------------------------------------------------------
enum Status
{
    S_OUTOFMEM  = -2,
    S_ERROR     = -1,
    S_OK        =  0,
};

//------------------------------------------------------------------------------------------------------------------------------
// List of camera APIs that could be used to access the camera
//------------------------------------------------------------------------------------------------------------------------------
enum CameraAPI
{
    CAMAPI_INVALID = -1,    ///< Invalid
    CAMAPI_V4L2,            ///< V4l2
    CAMAPI_HAL3,            ///< Hal3
    CAMAPI_MAX_TYPES        ///< Max types
};
const char* GetApiString(int api);

//------------------------------------------------------------------------------------------------------------------------------
// List of camera APIs that could be used to access the camera
//------------------------------------------------------------------------------------------------------------------------------
enum CameraMode
{
    CAMMODE_INVALID = -1,   ///< Invalid
    CAMMODE_PREVIEW,        ///< Preview
    CAMMODE_VIDEO,          ///< Video
    CAMMODE_SNAPSHOT,       ///< Snapshot
    CAMMODE_MAX_TYPES       ///< Max types
};
const char* GetCamModeString(int api);

// -----------------------------------------------------------------------------------------------------------------------------
// Supported preview formats
// -----------------------------------------------------------------------------------------------------------------------------
enum PreviewFormat
{
    PREVIEW_FMT_INVALID = -1,
    PREVIEW_FMT_RAW8    = 0,    ///< RAW8
    PREVIEW_FMT_RAW10,          ///< RAW10
    PREVIEW_FMT_NV12,           ///< NV12
    PREVIEW_FMT_NV21,           ///< NV21
    PREVIEW_FMT_BLOB,           ///< BLOB (tof)
    PREVIEW_FMT_MAXTYPES        ///< Max Types
};
const char* GetPreviewFmtString(int fmt);

// -----------------------------------------------------------------------------------------------------------------------------
// Supported video formats
// -----------------------------------------------------------------------------------------------------------------------------
enum VideoFormat
{
    VIDEO_FMT_INVALID = -1,
    VIDEO_FMT_H264    = 0,  ///< H264
    VIDEO_FMT_H265,         ///< H265
    VIDEO_FMT_MAXTYPES      ///< Max Types
};
const char* GetVideoFmtString(int fmt);

// -----------------------------------------------------------------------------------------------------------------------------
// Supported snapshot formats
// -----------------------------------------------------------------------------------------------------------------------------
enum SnapshotFormat
{
    SNAPSHOT_FMT_INVALID = -1,
    SNAPSHOT_FMT_JPG     = 0,   ///< Jpeg
    SNAPSHOT_FMT_MAXTYPES       ///< Max Types
};
const char* GetSnapshotFmtString(int fmt);

//------------------------------------------------------------------------------------------------------------------------------
// List of camera types
//------------------------------------------------------------------------------------------------------------------------------
enum CameraType
{
    CAMTYPE_INVALID = -1,   ///< Invalid
    CAMTYPE_TRACKING,       ///< Tracking camera
    CAMTYPE_HIRES,          ///< HiRes    camera
    CAMTYPE_TOF,            ///< ToF      camera
    CAMTYPE_STEREO,         ///< Stereo   camera
    CAMTYPE_MAX_TYPES       ///< Max types
};

// Get the string associated with the type
const char* GetTypeString(int type);

// Get the type associated with the string
const CameraType GetCameraTypeFromString(char *type);
//------------------------------------------------------------------------------------------------------------------------------
// List of AutoExposure modes
//------------------------------------------------------------------------------------------------------------------------------
enum CameraAEMode
{
    CAMAEMODE_INVALID = -1,     ///< Invalid
    CAMAEMODE_OFF,              ///< Auto exposure off
    CAMAEMODE_ON,               ///< Auto exposure on
    CAMAEMODE_MAX_TYPES         ///< Max types
};

//------------------------------------------------------------------------------------------------------------------------------
// Auto exposure algorithms
//------------------------------------------------------------------------------------------------------------------------------
enum CameraAEAlgo
{
    CAMAEALGO_INVALID = -1,     ///< Invalid
    CAMAEALGO_OFF,              ///< None i.e. maintain static values for manual exposure/gain
    CAMAEALGO_ISP,              ///< Underlying camera driver does the auto exposure/gain
    CAMAEALGO_MODALAI,          ///< OpenCV based histogram made by modalai (found in libmodal_exposure)
    CAMAEALGO_MAX_TYPES         ///< Max types
};
const char* GetAEString(int algo);

//------------------------------------------------------------------------------------------------------------------------------
// Structure containing information for one camera
// Any changes to this struct should be reflected in camera_defaults.h as well
//------------------------------------------------------------------------------------------------------------------------------
struct PerCameraInfo
{
    char          name[MAX_NAME_LENGTH]; ///< Camera name string
    char          port[MAX_NAME_LENGTH]; ///< Port on which the camera is
    CameraType    type;                  ///< Type of camera
    CameraAPI     accessApi;             ///< API used to access the camera
    int           fps;                   ///< Frame rate - number of frames per second
    int           tofMode;               ///< Tof Mode (1-11, currently only 5 and 9 supported)
                                         ///  see camera_defaults.cpp for specifics
    int           overrideId;            ///< Force using this camera-id
    bool          isEnabled;             ///< Is the camera enabled/disabled

    struct ModeInfo
    {
        bool isEnabled;          ///< Is this mode enabled
        int  width;              ///< Width of the frame
        int  height;             ///< Height of the frame
        int  format;             ///< Frame format
    } modeInfo[CAMMODE_MAX_TYPES];

    struct
    {
        int          exposureNs;         ///< Exposure time in nano seconds
        int          gain;               ///< ISO Gain
        CameraAEAlgo algorithm;          ///< Algorithm to use in case auto exposure is disabled
        modal_exposure_config_t modalai; ///< ModalAI AE data (from libmodal_exposure)
    } expGainInfo;
};

const int numRequiredOutputChannels[CAMTYPE_MAX_TYPES] = 
{
    1,                 ///< Tracking(preview)
    1,                 ///< Hires   (preview)
    NUM_TOF_CHANNELS,  ///< Tof     (IR, depth, confidence,noise, pointcloud)
    1                  ///< Stereo  (preview)
};
extern const char *tofChannelNames[NUM_TOF_CHANNELS];

extern const int tofPipeSizes[NUM_TOF_CHANNELS];

#endif // COMMON_DEFS
