/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include "common_defs.h"
#include "debug_log.h"
#include "hal3_camera.h"

// List the class static variables to make the compiler happy
int32_t          CameraHAL3::m_sNumCameras;                  ///< Number of cameras detected
int32_t          CameraHAL3::m_sCamIdTOF;                    ///< ToF camera id
int32_t          CameraHAL3::m_sCamIdHiRes;                  ///< HiRes camera id
int32_t          CameraHAL3::m_sCamIdStereo;                 ///< Stereo camera id
int32_t          CameraHAL3::m_sCamIdTracking;               ///< Tracking camera id
int32_t          CameraHAL3::m_sIsInitialized;               ///< Is HAL3 interface initialized
camera_info      CameraHAL3::m_sCameraInfo[MaxCameras];      ///< Per camera info
camera_module_t* CameraHAL3::m_spCameraModule;               ///< Camera module

// -----------------------------------------------------------------------------------------------------------------------------
// Constructor
// -----------------------------------------------------------------------------------------------------------------------------
CameraHAL3::CameraHAL3()
{
    m_moduleCallbacks = {CameraDeviceStatusChange, TorchModeStatusChange};

    m_pPerCameraMgr = NULL;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// @todo We need to make this static for multi-camera case because it is a one time functionality for all cameras
// -----------------------------------------------------------------------------------------------------------------------------
Status CameraHAL3::Initialize(ApiInterfaceData* pIntfData)
{
    int status = 0;

    if (pIntfData != NULL)
    {
        memcpy(&m_apiInterface, pIntfData, sizeof(ApiInterfaceData));
    }

    // We need to initialize the HAL3 interface only once for all cameras
    if (!m_sIsInitialized)
    {
        m_sCamIdTOF      = -1;
        m_sCamIdHiRes    = -1;
        m_sCamIdStereo   = -1;
        m_sCamIdTracking = -1;
        m_sIsInitialized = 0;

        status = hw_get_module(CAMERA_HARDWARE_MODULE_ID, (const hw_module_t**)&m_spCameraModule);

        if (status == 0)
        {
            VOXL_LOG_INFO("SUCCESS: Camera module opened\n");
        }
        else
        {
            VOXL_LOG_ERROR("ERROR: Camera module not opened, attempting again\n");

            //When systemctl begins this server at startup, there is a chance that the
            //  hardware layer is not ready, try a few times
            for (int i = 2; i <= NUM_MODULE_OPEN_ATTEMPTS; i++){

                status = hw_get_module(CAMERA_HARDWARE_MODULE_ID, (const hw_module_t**)&m_spCameraModule);
                if(status == 0){

                    VOXL_LOG_INFO("SUCCESS: Camera module opened on attempt %d\n", i);
                    break;

                }else{
                    VOXL_LOG_ERROR("ERROR: Camera module not opened, %d attempts remaining\n",
                                    NUM_MODULE_OPEN_ATTEMPTS-i+1);
                    usleep(MODULE_ATTEMPT_DELAY_MS*1000);
                }

            }
        }

        if(status != 0){
            VOXL_LOG_FATAL("ERROR: Camera module not opened after %d attempts, aborting", NUM_MODULE_OPEN_ATTEMPTS);
        }

        if (status == 0)
        {
            if (m_spCameraModule->init != NULL)
            {
                status = m_spCameraModule->init();
            }
        }

        if (status == 0)
        {
            m_sNumCameras = m_spCameraModule->get_number_of_cameras();

            VOXL_LOG_INFO("----------- Number of cameras: %d\n\n", m_sNumCameras);

            for (int i = 0 ;i < m_sNumCameras;i++)
            {
                // This gives the camera's fixed characteristics that can be extracted from the camera_metadata
                // "info.static_camera_characteristics"
                status = m_spCameraModule->get_camera_info(i, &m_sCameraInfo[i]);

                camera_metadata_t* pStaticMetadata = (camera_metadata_t *)m_sCameraInfo[i].static_camera_characteristics;
                camera_metadata_ro_entry entry;

                // Get the list of all stream resolutions supported and then go through each one of them looking for a match
                int status = find_camera_metadata_ro_entry(pStaticMetadata, ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS, &entry);

                // Monochrome camera detection information https://source.android.com/devices/camera/monochrome
                if ((0 == status) && (0 == (entry.count % 4)))
                {
                    int32_t maxWidth  = 0;
                    int32_t maxHeight = 0;
                    ///<@todo Need to find a better way to detect different cameras
                    static const int32_t MinHiResWidth  = 3840;
                    static const int32_t MaxMonoWidth   = 640;
                    static const int32_t MaxStereoWidth = 1280;
                    static const int32_t TOFHeights[]   = {865, 1557, 1903};

                    for (size_t j = 0; j < entry.count; j+=4)
                    {
                        // entry.data.i32[i+0] = format
                        // entry.data.i32[i+1] = width
                        // entry.data.i32[i+2] = height
                        if (ANDROID_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_OUTPUT == entry.data.i32[j + 3])
                        {
                            maxWidth  = (entry.data.i32[j+1] > maxWidth)  ? entry.data.i32[j+1] : maxWidth;
                            maxHeight = (entry.data.i32[j+2] > maxHeight) ? entry.data.i32[j+2] : maxHeight;
                        }
                    }

                    if (maxHeight == TOFHeights[0] || maxHeight == TOFHeights[1] || maxHeight == TOFHeights[2])
                    {
                        m_sCamIdTOF = i;
                    }
                    else if (maxWidth  >= MinHiResWidth)
                    {
                        m_sCamIdHiRes = i;
                    }
                    else if (maxWidth >= MaxStereoWidth)
                    {
                        m_sCamIdStereo = i;
                    }
                    else if (maxWidth <= MaxMonoWidth)
                    {
                        m_sCamIdTracking = i;
                    }
                }
            }

            if (m_sCamIdTOF != -1)
            {
                VOXL_LOG_INFO("----------- ToF    camera id: %d\n", m_sCamIdTOF);
            }
            if (m_sCamIdHiRes != -1)
            {
                VOXL_LOG_INFO("----------- HiRes  camera id: %d\n", m_sCamIdHiRes);
            }
            if (m_sCamIdStereo != -1)
            {
                VOXL_LOG_INFO("----------- Stereo camera id: %d\n", m_sCamIdStereo);
            }
            if (m_sCamIdTracking != -1)
            {
                VOXL_LOG_INFO("----------- Mono   camera id: %d\n", m_sCamIdTracking);
            }
        }

        if (status == 0)
        {
            status = m_spCameraModule->set_callbacks(&m_moduleCallbacks);
        }

        if (status == 0)
        {
            m_sIsInitialized = 1;
        }
    }

    ///<@todo Make it proper status
    return (Status) status;
}

// -----------------------------------------------------------------------------------------------------------------------------
// Performs any one time initialization. This function should only be called once.
// -----------------------------------------------------------------------------------------------------------------------------
Status CameraHAL3::Start(const PerCameraInfo* pPerCameraInfo)   ///< Information about the camera to start
{
    Status status   = S_OK;
    int    cameraid = -1;

    if (pPerCameraInfo != NULL)
    {
        if(pPerCameraInfo->type != CAMTYPE_TOF){
            m_pPerCameraMgr = new PerCameraMgr;
        }else{
            m_pPerCameraMgr = new PerCameraMgrTof;
        }

        if (m_pPerCameraMgr != NULL)
        {
            switch (pPerCameraInfo->type)
            {
                case CAMTYPE_TRACKING:
                    cameraid = m_sCamIdTracking;
                    break;
                case CAMTYPE_TOF:
                    cameraid = m_sCamIdTOF;
                    break;
                case CAMTYPE_HIRES:
                    cameraid = m_sCamIdHiRes;
                    break;
                case CAMTYPE_STEREO:
                    cameraid = m_sCamIdStereo;
                    break;
                default:
                    status = S_ERROR;
                    break;
            }

            if (cameraid != -1)
            {
                // Initialize the per camera manager
                ///<@todo Make everything Status not int as success/failure
                PerCameraInitData perCameraInitData = { 0 };

                perCameraInitData.pCameraModule  = m_spCameraModule;
                perCameraInitData.pHalCameraInfo = &m_sCameraInfo[cameraid];
                perCameraInitData.cameraid       = cameraid;
                perCameraInitData.pApiInterface  = &m_apiInterface;
                perCameraInitData.pPerCameraInfo = pPerCameraInfo;

                status = (Status)m_pPerCameraMgr->Initialize(&perCameraInitData);

                if (status == S_OK)
                {
                    // Start the camera which will start sending requests and processing results from the camera module
                    status = (Status)m_pPerCameraMgr->Start();
                }
            }
            else
            {
                status = S_ERROR;
            }
        }
        else
        {
            status = S_ERROR;
        }
    }
    else
    {
        status = S_ERROR;
    }

    return status;
}
