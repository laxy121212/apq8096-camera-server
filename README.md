# apq8096-camera-server

Use HAL3 to get camera and PMD TOF frames and publish to modal pipes.

Use instructions [here](https://docs.modalai.com/voxl-camera-server/)

## Build dependencies

- libmodal_json
- libmodal_pipe
- libmodal_exposure


## Build Environment

NOTE: v0.5.0+ of camera server requires voxl system image 3.2+ to run and voxl-emulator build 1.5+ to build [(details)](https://docs.modalai.com/voxl-system-image/)


## Build Instructions

1. Requires the voxl-emulator (found [here](https://gitlab.com/voxl-public/voxl-docker)) to run docker ARM image
    * (PC) cd [Path To]/apq8096-camera-server
    * (PC) sudo voxl-docker -i voxl-emulator
2. Build project binary:
    * (VOXL-EMULATOR) ./install_build_deps.sh apq8096 staging
    * (VOXL-EMULATOR) ./clean.sh
    * (VOXL-EMULATOR) ./build.sh
    * (VOXL-EMULATOR) ./make_package.sh ipk
