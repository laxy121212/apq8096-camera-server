#!/bin/bash
################################################################################
# Copyright (c) 2020 ModalAI, Inc. All rights reserved.
################################################################################

# no need for arguments, this only builds in voxl-emulator for one platform

mkdir -p build
cd build
cmake ../
make -j$(nproc)
cd ../
